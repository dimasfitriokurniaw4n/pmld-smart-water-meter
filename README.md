
# PMLD - SMART WATER METER
Smart water meter using Ardunio Nano and LoRA module. The LoRA network used in this project provided by Antares.

## Installation
### .:Antares:.
- Sign Up at Antares Website.
- Create Application.
- Create Device.
- Setup LoRA Device (ABP/OTAA).
- Take note the Access Key in `Account > Copy Access Key Value` & Device Address in `Applications > YOUR-APPLICATION-NAME > YOUR-DEVICE-NAME > Copy LoRa.id Device Address Value`.

### .:Library:.
1. Download `.ZIP` file from this Repository.
>This library are based on Official Antares LoRA Library. The difference is it contains custom pin mapping specifically for this project.
2. Install from Arduino IDE Library Manager `Sketch > Include Library > Add .ZIP Library...`.  

### .:Upload Sketch:.
1. Replace Access Key & Device Address inside sketch with your own.
2. Upload the modified sketch.

## Pin Wiring
Pin Wiring Overview. For more detailed schematic, please see [this schematic](https://gitlab.com/dimasfitriokurniaw4n/pmld-smart-water-meter/-/blob/main/skematik.png).

### RFM95 LoRA Module -> Arduino Nano
| RFM95 | Arduino Nano|
|--|--|
| MISO | MISO (12) |
| MOSI | MOSI (11) |
| SCK | SCK (13) |
| NSS | NSS (10) |
| RST | 5 |
|3.3V | OUT (Step Down Module)  |
| GND | GND |
| DIO0 | 2 |
| DIO1 | 3 |

### Water Flow Sensor -> Arduino Nano
| Water Flow Sensor | Arduino Nano|
|--|--|
| VCC | 5V |
| DATA | 8 |
| GND | GND |

### Temperature & Humidity Sensor -> Arduino Nano
| Temperature & Humidity Sensor | Arduino Nano|
|--|--|
| VIN | OUT (Step Down Module) |
| SCL| A5 |
| SDA| A4 |
| GND | GND |

### MOSFET -> Arduino Nano
| MOSFET | Arduino Nano|
|--|--|
| Gate | 7 |
| Source | GND |

### Battery -> Arduino Nano (Battery Monitoring)
| Battery | Arduino Nano|
|--|--|
| + | A0 |
| - | GND |

### Step Down Module -> Arduino Nano
| Step Down Module| Arduino Nano|
|--|--|
| VIN | 5V |
| GND | GND |

## Program Flow
```mermaid
sequenceDiagram
participant Solenoid Valve
participant Water Flow Sensor
participant Temperature & Humidity Sensor
participant Battery
participant Arduino Nano
participant LoRA Module
participant Antares LoRA Platform

Water Flow Sensor->>Arduino Nano: Send Pulse
Arduino Nano-->>Temperature & Humidity Sensor: Read Sensor Data
Arduino Nano-->>Battery: Read Analog Value
Arduino Nano->>LoRA Module: Send Data using SPI
LoRA Module->>Antares LoRA Platform: Send LoRA Uplink Data
Antares LoRA Platform-->>LoRA Module: Send LoRA Downlink Data
LoRA Module-->>Arduino Nano: Send Data using SPI
Arduino Nano-->>Solenoid Valve: Open/Close Solenoid
```

## Uplink Key Meaning
| Key | Meaning |
|--|--|
| vs | Valve State |
| bs | Battery State |
| wu | Total Usage |
| ct | Current Temperature |
| ch | Current Humidity |
