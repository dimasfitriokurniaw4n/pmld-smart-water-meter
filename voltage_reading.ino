char cVoltage[8];
float voltage = 0.00;
int raw = 0;
unsigned long oldTime = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(A0, INPUT_PULLUP);
}

void loop() {
  // put your main code here, to run repeatedly:
  char buf[255];
  
  if ((millis() - oldTime) > 1000) {
    // Read voltage from A0
    raw = analogRead(A0);
    if (raw > 990) {
      voltage = 0.00;
    }
    else {
      voltage = (raw/1024.00)*4.75;
    }
    
    // Convert float to string
    dtostrf(voltage, 4, 2, cVoltage);
    
    // Print to Serial
    sprintf(buf, "V: %s, RAW: %d", cVoltage, raw);
    Serial.println(buf);
    
    // Reset oldTime
    oldTime = millis();
  }

}
