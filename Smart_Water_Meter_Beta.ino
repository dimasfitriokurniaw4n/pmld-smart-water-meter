#include <ArduinoJson.h>
#include <loraid.h>
#include <Wire.h>
#include <AHT10.h>


// Retry variables
int i = 0;
int state = 1;
int retry = 2;
long previousMillis = 0;
long retryInterval = 5000;
long wait = 0;

// Lora variables
char downStr[128];
char upStr[64];
int recvStatus = 0;
long loraInterval = 60000;
unsigned int counter = 0;

// Water flow variables
char cTotal[10];
float calibrationFactor = 7.5;
float flowRate = 0;
float total = 0;
long waterInterval = 1000;
long previousWaterMillis = 0;
volatile byte pulseCount = 0;

// Solenoid valve variables
bool lastState = 0;
bool currentState = 0;
char cValveState[4];
int valvePin = 7;

// Battery variables
char cBatteryState[4];
int raw = 0;

// Temperature & humidity variables
AHT10 aht10(AHT10_ADDRESS_0X38);
char cTemp[8];
char cHum[8];


// Battery state reading
void batteryReading() {
  raw = analogRead(A0);
  if (raw > 910) {
    strcpy(cBatteryState, "ERR");
  }
  else if (raw < 755) {
    strcpy(cBatteryState, "LOW");
  }
  else {
    strcpy(cBatteryState, "OK");
  }
}

// Insterrupt Service Routine
ISR(PCINT0_vect) {
  // Increment the pulse counter
  pulseCount++;
}

// Send uplink
void sendUplink() {  
  if (wait > 0) {
    if ((millis() - previousMillis) < wait) {
      return;
    }
    else {
      wait = 0;
    }
  }
  
  switch (state) {
    case 0:
      previousMillis = millis();
      wait = loraInterval;
      state = 1;
      break;

    case 1:
      // Battery state reading
      batteryReading();

      // Temperature & Humidity reading
      dtostrf(aht10.readTemperature(), 1, 2, cTemp);
      dtostrf(aht10.readHumidity(), 1, 2, cHum);
      
      if (total != 0) {
        sprintf(upStr, "{\"vs\":\"%s\", \"bs\":\"%s\", \"wu\":\%s\}", cValveState, cBatteryState, cTotal);
      }
      else {
        sprintf(upStr, "{\"vs\":\"%s\", \"bs\":\"%s\", \"ct\":\%s\, \"ch\":\%s\}", cValveState, cBatteryState, cTemp, cHum);
      }
      
      // Retry send uplink every retryInterval
      lora.sendToAntares((unsigned char *)upStr, strlen(upStr), 0);
//      counter++;
      i++;
      if (i < retry) {
        state = 1;
        previousMillis = millis();
        wait = retryInterval;
      }
      else {
        memset(upStr, 0, sizeof(upStr));
        state = 0;
        i = 0;
        total = 0;
      }
      break;
  }
}

void setup() {
  // Setup loraid access
  lora.init();

  // Set lorawan class
  lora.setDeviceClass(CLASS_C);

  // Set data rate
  lora.setDataRate(2);

  // Set Tx Power
  lora.setTxPower(17);

  // Put antares key and devaddress here
  lora.setAccessKey("b49c021d69c7934e:ce0710b2ad7f5cc3");
  lora.setDeviceId("731b843d");

  // Pin Change (Interrupt) Register
  PCMSK0 |= bit(PCINT0);
  PCIFR |= bit(PCIF0);
  PCICR |= bit(PCIE0);
  
  // Solenoid valve pin setup
  pinMode(valvePin, OUTPUT);
  digitalWrite(valvePin, LOW);
  strcpy(cValveState, "OFF");

  // Analog input pin pullup
  pinMode(A0, INPUT_PULLUP);

  // Temperature & humidity sensor initialization
  aht10.begin();
}

void loop() {  
  // Send uplink at determined interval
  sendUplink();

  // Check lora rx
  lora.update();
  
  // Check downlink data
  recvStatus = lora.readData(downStr);
  if (recvStatus) {
    
    // Parse JSON-formatted downlink
    StaticJsonDocument<128> doc;
    DeserializationError error = deserializeJson(doc, downStr);
    
    const char* solenoidState = doc["vs"];
    
    // Trigger the solenoid valve
    if (strcmp(solenoidState, "ON") == 0) {
      digitalWrite(valvePin, HIGH);
      strcpy(cValveState, "ON");
    }
    else if (strcmp(solenoidState, "OF") == 0) {
      digitalWrite(valvePin, LOW);
      strcpy(cValveState, "OFF");
    }
    
    // Check valvePin state
    currentState = digitalRead(valvePin);
    if (currentState != lastState) {
      lastState = currentState;
      state = 1;
      wait = 0;
    }
  }

  // Water flow calculation
  if ((millis() - previousWaterMillis) > waterInterval) {
    // Total in meter cubic
    total += (((1000.00 / (millis() - previousWaterMillis)) * pulseCount) / calibrationFactor) / 60000;
    previousWaterMillis = millis();
    
    // Reset the pulse counter
    pulseCount = 0;

    // Convert float to string
    dtostrf(total, 1, 6, cTotal);
  } 
}
